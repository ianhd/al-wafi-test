<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('food', 'API\ApiFoodController@index');
Route::post('food', 'API\ApiFoodController@create');
Route::get('/food/{id}', 'API\ApiFoodController@show');
Route::put('/food/{id}', 'API\ApiFoodController@update');
Route::delete('/food/{id}', 'API\ApiFoodController@destroy');