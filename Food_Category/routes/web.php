<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/template', function () {
    return view('template.master');
});

Route::get('/Food', function () {
    return view('admin.food');
})->name('admin_food');

Route::get('/', 'FoodController@getFood_Index')->name('Food_getIndex');
Route::get('/list', 'FoodController@getFood_List')->name('Food_getList');
Route::get('/tambah', 'FoodController@getFood_Input')->name('Food_getInput');
Route::post('/tambah', 'FoodController@postFood_Input')->name('Food_postInput');
Route::get('/ubah', 'FoodController@getFood_Update')->name('Food_getUpdate');
Route::post('/ubah', 'FoodController@postFood_Update')->name('Food_postUpdate');
Route::post('/hapus', 'FoodController@postFood_Delete')->name('Food_postDelete');
Route::get('/listjson', 'FoodController@getFood_Listjson')->name('Food_getListjson');