@extends('template.master')

@section('css')
<style>
    /* @import url("https://fonts.googleapis.com/css?family=Roboto"); */

    /* html {
        height: 100%;
        width: 100%;
    } */

    p {
        margin: 0;
    }

    /* body {
        height: 100%;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    } */

    .bt {
        width: 100px;
        cursor: pointer;
        position: relative;
        font-family: "Roboto";
        text-transform: uppercase;
        color: #503af6;
        letter-spacing: 0.5px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        outline: none;
        text-decoration: none;
        text-align: center;
    }

    .more-bt {
        border-right: 2px solid #503af6;
        border-bottom: 2px solid #503af6;
        border-right: 2px solid #503af6;
        border-bottom: 2px solid #503af6;
        padding: 17px 29px 15px 31px;
        border-color: #503af6;
    }

    .more-bt p {
        font-size: 14px;
    }

    #wrapper.smooth section.smoothy.show {
        visibility: visible;
    }

    .more-bt:before {
        left: 0;
        bottom: 0;
        height: -webkit-calc(100% - 17px);
        height: calc(100% - 17px);
        width: 2px;
    }

    .more-bt:after,
    .more-bt:before {
        content: " ";
        display: block;
        background: #503af6;
        position: absolute;
        -webkit-transition: 0.5s;
        transition: 0.5s;
        z-index: 10;
    }

    .more-bt:after {
        top: 0;
        right: 0;
        width: -webkit-calc(100% - 17px);
        width: calc(100% - 17px);
        height: 2px;
    }

    .more-bt:after,
    .more-bt:before {
        content: " ";
        display: block;
        background: #503af6;
        position: absolute;
        -webkit-transition: 0.5s;
        transition: 0.5s;
        z-index: 10;
    }

    ::selection {
        background: #503af6;
        color: #ffffff;
        text-shadow: none;
    }

    .more-bt:before {
        left: 0;
        bottom: 0;
        height: -webkit-calc(100% - 17px);
        height: calc(100% - 17px);
        width: 2px;
    }

    .more-bt:after,
    .more-bt:before {
        content: " ";
        display: block;
        background: #503af6;
        position: absolute;
        -webkit-transition: 0.5s;
        transition: 0.5s;
        z-index: 10;
    }

    .more-bt .fl,
    .more-bt .sfl {
        position: absolute;
        right: 0;
        height: 100%;
        width: 0;
        z-index: 2;
        background: #503af6;
        top: 0;
        -webkit-transition: 0.5s;
        transition: 0.5s;
        -webkit-transition-delay: 0.1s;
        transition-delay: 0.1s;
    }

    .more-bt .fl,
    .more-bt .sfl {
        position: absolute;
        right: 0;
        height: 100%;
        width: 0;
        z-index: 2;
        background: #503af6;
        top: 0;
        -webkit-transition: 0.5s;
        transition: 0.5s;
        -webkit-transition-delay: 0.1s;
        transition-delay: 0.1s;
    }

    .more-bt .sfl {
        z-index: 1;
        background: #4431d1;
        -webkit-transition: 0.7s;
        transition: 0.7s;
    }

    .more-bt .cross {
        position: absolute;
        z-index: 15;
        width: 18px;
        height: 18px;
        top: -webkit-calc(50% - 8px);
        top: calc(50% - 8px);
        left: -webkit-calc(50% - 8px);
        left: calc(50% - 8px);
    }

    #wrapper.smooth section.smoothy {
        visibility: hidden;
    }

    .more-bt .cross:before {
        width: 100%;
        height: 2px;
        top: 8px;
        left: 0px;
        -webkit-transform: translateX(50px) scaleX(0);
        -ms-transform: translateX(50px) scaleX(0);
        transform: translateX(50px) scaleX(0);
    }

    .more-bt .cross:before,
    .more-bt .cross:after {
        content: " ";
        background: #fff;
        display: block;
        position: absolute;
        opacity: 0;
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
        -webkit-transition-delay: 0s;
        transition-delay: 0s;
        -webkit-transition-timing-function: cubic-bezier(0.86, 0, 0.07, 1);
        transition-timing-function: cubic-bezier(0.86, 0, 0.07, 1);
    }

    .more-bt .cross:after {
        width: 2px;
        height: 100%;
        left: 8px;
        top: 0;
        -webkit-transform: translateY(20px) scaleY(0);
        -ms-transform: translateY(20px) scaleY(0);
        transform: translateY(20px) scaleY(0);
        -webkit-transition-duration: 0.4s;
        transition-duration: 0.4s;
    }

    .more-bt .cross:before,
    .more-bt .cross:after {
        content: " ";
        background: #fff;
        display: block;
        position: absolute;
        opacity: 0;
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
        -webkit-transition-delay: 0s;
        transition-delay: 0s;
        -webkit-transition-timing-function: cubic-bezier(0.86, 0, 0.07, 1);
        transition-timing-function: cubic-bezier(0.86, 0, 0.07, 1);
    }

    .more-bt i {
        position: absolute;
        display: block;
        top: 1px;
        left: 1px;
        -webkit-transition: 0.5s;
        transition: 0.5s;
        z-index: 10;
    }

    .more-bt i:before {
        -webkit-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
    }

    .more-bt i:after,
    .more-bt i:before {
        content: " ";
        display: block;
        width: 2px;
        height: 20px;
        background: #503af6;
        position: absolute;
        margin: -10px -1px;
        left: 50%;
        top: 50%;
        transition: 0.3s;
    }

    .more-bt:hover i:after {
        content: " ";
        display: block;
        width: 2px;
        height: 20px;
        background: #503af6;
        position: absolute;
        margin: 0px -1px;
        left: 50%;
        top: 50%;
    }

    .more-bt:hover i:before {
        content: " ";
        display: block;
        width: 2px;
        height: 20px;
        background: #503af6;
        position: absolute;
        margin: -10px 0px -10px 8px;
        left: 50%;
        top: 50%;
    }

    .more-bt p {
        -webkit-transition: 0.5s;
        transition: 0.5s;
        position: relative;
        z-index: 1;
    }

    .more-bt:hover:before,
    .more-bt:before {
        height: 100%;
    }

    .more-bt:before {
        left: 0;
        bottom: 0;
        height: -webkit-calc(100% - 17px);
        height: calc(100% - 17px);
        width: 2px;
    }

    .more-bt:hover .fl,
    .more-bt .fl {
        -webkit-transition: 0.7s;
        transition: 0.7s;
    }

    .more-bt:hover .fl,
    .more-bt:hover .sfl,
    .more-bt .fl,
    .more-bt .sfl {
        -webkit-transition-delay: 0s;
        transition-delay: 0s;
        width: 100%;
    }

    .more-bt .fl,
    .more-bt .sfl {
        position: absolute;
        right: 0;
        height: 100%;
        width: 0;
        z-index: 2;
        background: #503af6;
        top: 0;
        -webkit-transition: 0.5s;
        transition: 0.5s;
        -webkit-transition-delay: 0.1s;
        transition-delay: 0.1s;
    }

    .more-bt:hover .sfl,
    .more-bt. .sfl {
        -webkit-transition: 0.5s;
        transition: 0.5s;
    }

    .more-bt:hover .fl,
    .more-bt:hover .sfl,
    .more-bt.hvd .fl,
    .more-bt.hvd .sfl {
        -webkit-transition-delay: 0s;
        transition-delay: 0s;
        width: 100%;
    }

    .more-bt .sfl {
        z-index: 1;
        background: #4431d1;
        -webkit-transition: 0.7s;
        transition: 0.7s;
    }

    .more-bt .cross {
        position: absolute;
        z-index: 15;
        width: 18px;
        height: 18px;
        top: -webkit-calc(50% - 8px);
        top: calc(50% - 8px);
        left: -webkit-calc(50% - 8px);
        left: calc(50% - 8px);
    }

    .more-bt:hover .cross:before,
    .more-bt .cross:before {
        -webkit-transition-duration: 0.5s;
        transition-duration: 0.5s;
    }

    .more-bt:hover .cross:after,
    .more-bt:hover .cross:before,
    .more-bt .cross:after,
    .more-bt .cross:before {
        -webkit-transform: none;
        -ms-transform: none;
        transform: none;
        opacity: 1;
        -webkit-transition-delay: 0.2s;
        transition-delay: 0.2s;
    }

    .more-bt .cross:after {
        width: 2px;
        height: 100%;
        left: 8px;
        top: 0;
        -webkit-transform: translateY(20px) scaleY(0);
        -ms-transform: translateY(20px) scaleY(0);
        transform: translateY(20px) scaleY(0);
        -webkit-transition-duration: 0.4s;
        transition-duration: 0.4s;
    }

    .more-bt .cross:before,
    .more-bt .cross:after {
        content: " ";
        background: #fff;
        display: block;
        position: absolute;
        opacity: 0;
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
        -webkit-transition-delay: 0s;
        transition-delay: 0s;
        -webkit-transition-timing-function: cubic-bezier(0.86, 0, 0.07, 1);
        transition-timing-function: cubic-bezier(0.86, 0, 0.07, 1);
    }

    .more-bt:hover .cross:after,
    .more-bt:hover .cross:before,
    .more-bt.hvd .cross:after,
    .more-bt.hvd .cross:before {
        -webkit-transform: none;
        -ms-transform: none;
        transform: none;
        opacity: 1;
        -webkit-transition-delay: 0.2s;
        transition-delay: 0.2s;
    }

    .more-bt:hover .cross:after,
    .more-bt.hvd .cross:after {
        -webkit-transition-duration: 0.6s;
        transition-duration: 0.6s;
    }

</style>
@endsection

@section('content')

<!-- Content Start Here  -->
<div style="margin-bottom: 0.2rem;" class="col-12 grid-margin">
    <div style="height: 65px; border-radius: 15px;" class="card">
        <div class="card-body">

            <div style="display: flex; align-items: flex-start; justify-content: space-between;">
                <h4 class="card-title">FOOD</h4>
                {{-- <button type="button" class="btn btn-success btn-rounded btn-icon" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-success" title="EDIT" data-target="#showModalUpdateFood" style="margin-right:5px;"><i style="font-size:1.5rem; margin-left:-7px;" class="ti-pencil-alt"></i></button> --}}
                {{-- <button style="margin: -0.2rem .05rem -1rem auto;" type="button" class="btn btn-primary btn-sm"
                    data-toggle="modal" data-target="#showModalInsertFood"><i class="fas fa-plus"></i>&ensp;
                    Add</button> --}}
                <a class="bt more-bt" href="javascript:void(0)" data-toggle="modal" data-target="#showModalInsertFood"
                    style="padding-bottom: 4px; padding-top: 7px; padding-left: 29px; bottom: 6px;">
                    <span class="fl"></span>
                    <span class="sfl"></span>
                    <span class="cross"></span>
                    <i></i>
                    <p>add</p>
                </a>
            </div>

            <!-- Modal Starts Insert Data -->
            <div class="modal fade" id="showModalInsertFood" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel-2" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel-2">FOOD FORM</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form class="forms-sample" method="POST" action="#" id="formAddFood">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <div class="modal-body">
                                <div style="flex: 30%; max-width: 45%;" class="form-group">
                                    <label>NAMA</label>
                                    <input type="text" class="form-control form-control-sm" maxlength="100"
                                        name="newName" id="name" placeholder="NAMA"
                                        aria-label="NAMAE" >
                                </div>
                                <div class="form-group">
                                    <label>TYPE</label>
                                    <select class="form-control form-control-sm col-sm-6" name="newType" id="type">
                                        <option value="Appetizer">APPETIZER</option>
                                        <option value="main-course">MAIN COURSE</option>
                                        <option value="dessert">DESSERT</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">SUBMIT</button>
                                <button type="button" class="btn btn-light" data-dismiss="modal"
                                    id="btnCloseModalInsert">CANCEL</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Modal End Insert Data -->

            <!-- Modal Start Update Data -->
            <div class="modal fade" id="showModalUpdateFood" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel-2" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel-2">UPDATE FOOD</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form class="forms-sample" method="POST" action="#" id="formUpdateFood">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
                            <input type="hidden" class="form-control" id="id" name="id" placeholder="NAMA"
                                required>
                            <div class="modal-body">
                                <div style="flex: 30%; max-width: 45%;" class="form-group">
                                    <label>NAMA</label>
                                    <input type="text" class="form-control form-control-sm" maxlength="100"
                                        name="updateName" id="idUpdateName"
                                        aria-label="NAMA">
                                </div>
                                <div class="form-group">
                                    <label>TYPE</label>
                                    <select class="form-control form-control-sm col-sm-6" name="updateType" id="idUpdateType">
                                        <option value="Appetizer">APPETIZER</option>
                                        <option value="main-course">MAIN COURSE</option>
                                        <option value="dessert">DESSERT</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" id="btnUpdate">UPDATE</button>
                                <button type="button" class="btn btn-light" data-dismiss="modal"
                                    id="btnCloseModalUpdate">CANCEL</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Modal End Update Data -->
        </div>
    </div>
</div>
<!-- Content End -->

<!-- Table -->
<div class="col-12 grid-margin stretch-card">
    <div class="card" style="border-radius: 15px;">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table id="tableFood" class="table">
                            <thead>
                                <tr>
                                    <th style="width: 200px;">NAMA</th>
                                    <th style="width: 800px;">TYPE</th>
                                    <th style="padding-left: 30px;">Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Table End -->

@endsection

@section('js')
<!-- Form -->
<script src="{{ asset('js/file-upload.js') }}"></script>
{{-- <script src="{{ asset('js/iCheck.js') }}"></script> --}}
<script src="{{ asset('js/typeahead.js') }}"></script>
<script src="{{ asset('js/select2.js') }}"></script>

<!-- Table -->
<script src="{{ asset('js/data-table.js') }}"></script>
<script src="{{ asset('js/form-validation.js') }}"></script>
<script src="{{ asset('js/bt-maxLength.js') }}"></script>
<script src="{{ asset('js/toastDemo.js') }}"></script>
<script src="{{ asset('js/desktop-notification.js') }}"></script>

<!-- Tooltip -->
<script src="{{ asset('js/tooltips.js') }}"></script>
<script src="{{ asset('js/popover.js') }}"></script>

@endsection


@section('jscostume')
<script>
    var mTable;

    $(document).ready(function () {
        
        mTable = $('#tableFood').DataTable({
            responsive: true,
            processing: true,
            "language": {
                "processing": "<div class='circle-loader'></div>"
            },
            serverSide: true,
            ajax: "{{ route('Food_getList')}}",
            columns: [
                
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'type',
                    name: 'type'
                },
                {
                    data: 'id',
                    render: function (data, type, row) {
                        // console.log(type);
                        let buttonEdit =
                            '<button type="button" class="btn btn-success btn-rounded btn-icon" data-toggle="modal" data-placement="buttom" data-custom-class="tooltip-success" title="EDIT" data-target="#showModalUpdateFood" style="margin-right:5px;" onclick="buttonEdit(\'' +
                            data +
                            '\');"><i style="font-size:1.5rem; margin-left:-7px;" class="ti-pencil-alt"></i></button>';
                        // let buttonEdit = '<button type="button" class="btn-sm btn-info" data-toggle="modal" data-target="#showModalUpdate" style="margin-right:5px;" onclick="buttonEdit(\''+data+'\');">Update</button>';
                        let buttonHapus =
                            '<button type="button" class="btn btn-danger btn-rounded btn-icon" data-toggle="tooltip" data-placement="bottom" data-custom-class="tooltip-success" title="DELETE" onclick="buttonDelete(\'' +
                            data +
                            '\');"><i style="font-size:1.5rem; margin-left:-8px;" class="ti-trash"></i></button>';
                        return buttonEdit + buttonHapus;
                    }
                }
            ]
        });
    });

    $('#formAddFood').on('submit', function (e) {
        
        $('#btnCloseModalInsert').click();
        
        e.preventDefault();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        
        $.ajax({
            
            type: "POST",
            
            url: "{{ route ('Food_postInput') }}",
            
            data: $(this).serialize(),
            
            success: function (response) {
                $('#btnCloseModalInsert').click();
                if (response.status == 200) {
                    $('#formAddFood').trigger("reset");
                    mTable.ajax.reload();
                    $.toast({
                        heading: 'Success',
                        text: response.message,
                        showHideTransition: 'slide',
                        icon: 'success',
                        loaderBg: '#f96868',
                        position: 'top-right'
                    })
                    $('#btnCloseModalInsert').click();
                } else {
                    $.toast({
                        heading: 'Danger',
                        text: response.message,
                        showHideTransition: 'slide',
                        icon: 'error',
                        loaderBg: '#f2a654',
                        position: 'top-right'
                    })
                    $('#btnCloseModalInsert').click();
                }
            }
        });
        
    });

    
    function buttonEdit(idx) {
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        $.ajax({
            type: "GET",
            url: "{{ route('Food_getUpdate') }}",
            data: {
                id: idx
            },
            success: function (datax) {
                
                if (datax) {
                    $("#id").val(datax.id);
                    $("#idUpdateName").val(datax.name);
                    $("#idUpdateType").val(datax.type);
                } else {
                    
                }
            }
        });
    }

    
    $('#formUpdateFood').on('submit', function (e) {

        $('#btnCloseModalUpdate').click();
        
        e.preventDefault();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        
        $.ajax({
            
            type: "POST",
            
            url: "{{ route ('Food_postUpdate') }}",
            
            data: $(this).serialize(),
            
            success: function (response) {
                
                if (response.status == 200) {
                    $('#formUpdateFood').trigger("reset");
                    mTable.ajax.reload();
                    $.toast({
                        heading: 'Success',
                        text: response.message,
                        showHideTransition: 'slide',
                        icon: 'success',
                        loaderBg: '#f96868',
                        position: 'top-right'
                    })
                    $('#btnCloseModalInsert').click();
                } else {
                    $.toast({
                        heading: 'Danger',
                        text: response.message,
                        showHideTransition: 'slide',
                        icon: 'error',
                        loaderBg: '#f2a654',
                        position: 'top-right'
                    })
                    $('#btnCloseModalInsert').click();
                }
            }
        });
        
    });

    function buttonDelete(idx) {
        swal({
                title: "Apakah Kamu Yakin?",
                text: "Klik OK, Maka Data Kamu Tidak Dapat DiKembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $("input[name='_token']").val()
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "{{ route('Food_postDelete') }}",
                        data: {
                            id: idx
                        },
                        success: function (data) {
                            // console.log
                            mTable.ajax.reload();
                            $.toast({
                                heading: 'Danger',
                                text: 'Data Food Berhasil di Hapus',
                                showHideTransition: 'slide',
                                icon: 'error',
                                loaderBg: '#f2a654',
                                position: 'top-right'
                            })
                            $('#btnCloseModalInsert').click();
                        },
                        error: function (data) {
                            console.log('Error:', data.massage);
                            $('#btnCloseModalInsert').click();
                        }
                    });
                } else {
                    $.toast({
                        heading: 'Warning',
                        text: 'Batal Menghapus Data',
                        showHideTransition: 'slide',
                        icon: 'warning',
                        loaderBg: '#57c7d4',
                        position: 'top-right'
                    })
                    $('#btnCloseModalInsert').click();
                }
            });
    }

</script>
@endsection
