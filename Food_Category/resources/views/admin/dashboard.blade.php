@extends('template.master')

@section('css')

@endsection

@section('content')
<div class="col-md-12 grid-margin stretch-card">
    <div class="card position-relative">
        <div class="card-body">
            <p class="card-title">Aye Aye ...</p>
            <iframe
			  width="560" height="315" src="https://www.youtube.com/watch?v=IHNzOHi8sJs"
			  srcdoc="<style>
						*{padding:0;margin:0;overflow:hidden}html,body{height:100%}img,span{position:absolute;width:100%;top:0;bottom:0;margin:auto}span{height:1.5em;text-align:center;font:48px/1.5 sans-serif;color:white;text-shadow:0 0 0.5em black}
					</style><a href=https://www.youtube.com/watch?v=IHNzOHi8sJs?autoplay=1><img src=https://i.pinimg.com/originals/6f/13/e6/6f13e6f55de8fdb323bdc8ca8f604d79.jpg alt='Black Pink is The Revolution'><span>▶</span></a>"
			  frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen
			  title="The Dark Knight Rises: What Went Wrong? – Wisecrack Edition"
			></iframe>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('js/dashboard.js') }}"></script>
@endsection

@section('jscustome')

@endsection
