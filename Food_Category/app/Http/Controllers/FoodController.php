<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Models\Food;
use Yajra\Datatables\Datatables;

class FoodController extends Controller
{
    public function getFood_Index(){
        return view('admin.food');
    }

    public function getFood_List(){
        $res = Food::get();
        return Datatables::of($res)->make(true);
    }

    public function getFood_Listjson(){
        $res = Food::get();
        return response()->json($res);
    }

    public function postFood_Input(Request $request){
        $name    = $request->input('newName');
        $type    = $request->input('newType');
        
        $validator = Validator::make($request->all(), [
            'newName'      => 'required|min:2',
            'newType'      => 'required|max:255'
        ]);

        if ($validator->fails()) 
        {
            return response()->json(['status'=>422,'message'=>'Name atau Type harus lebih 2 karakter']);
        }
        else
        {
            $fd = new Food;
            $fd->name = $name;
            $fd->type = $type;
            $fd->save();
            return response()->json(['status'=>200,'message'=>'Data Berhasil DiInput']);

        }
        return response()->json(['status'=>422,'message'=>$validator->messages()]);
    }

    public function getFood_Update(){
        $id = request()->query('id');
        $Fd = Food::find($id);
        return response()->json($Fd);
    }

    public function postFood_Update(Request $request){
        //dd($request->all());
        $id = request()->input('id'); 
        $name    = $request->input('updateName');
        $type    = $request->input('updateType');
        $validator = Validator::make($request->all(), [
            
            'updateName' => 'required|min:2',
            'updateType' => 'required|max:255'
        ]);

        if ($validator->fails()) 
        {
            return response()->json(['status'=>422,'message'=>$validator->messages()]);
        }
        else
        {
            $cekkodeall = Food::where('name', $name);

            $Fd = Food::find($id);
            $Fd->name = $name;
            $Fd->type = $type;
            $Fd->save();

            return response()->json(['status'=>200,'message'=>'Data Berhasil Dirubah']);            
        }
    }

    public function postFood_Delete(){
        $id = request()->input('id');
        $Fd = Food::find($id);
        $Fd->delete();
    }
}
