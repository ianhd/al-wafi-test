<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $table = 'foods';
    public $fillable = [ 
        'name', 
        'type'
    ];
    public $timestamps = true;
}
