<?php

use Illuminate\Database\Seeder;
use App\Models\Food;

class FoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataName = ['Scramble Egg', 'Takoyaki', 'Karedok', 'Pempek Palembang', 'Nasi Goreng Bebek', 'Tenpayaki'];
        $dataType = ['Appetizer','main-course','dessert', 'main-course','dessert', 'Appetizer'];
        for ($n=0; $n<6; $n++) { 
            # code...
            $m = $n + 1;
            $fd = new Food;
            $fd->name  = $dataName[$n];
            $fd->type  = $dataType[$n];
            $fd->save();
        }
    }
}
