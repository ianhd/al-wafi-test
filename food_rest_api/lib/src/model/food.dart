import 'dart:convert';

class Food {
  int id;
  String nama;
  String type;

  Food({this.id=1, this.nama, this.type});

  factory Food.fromJson(Map<String, dynamic> map) {
    return Food(
        id: map["id"], nama: map["nama"], type: map["type"]);
  }

  Map<String, dynamic> toJson() {
    return {"id": id, "nama": nama, "type": type};
  }

  @override
  String toString() {
    return '{id: $id, nama: $nama, type: $type}';
  }

}

List<Food> foodFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Food>.from(data.map((item) => Food.fromJson(item)));
}

String foodToJson(Food data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
