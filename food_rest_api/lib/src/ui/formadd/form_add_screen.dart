import 'package:flutter/material.dart';
import 'package:food_rest_api/src/api/api_service.dart';
import 'package:food_rest_api/src/model/food.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class FormAddScreen extends StatefulWidget {
  Food fds;
  FormAddScreen({this.fds});

  @override
  _FormAddScreenState createState() => _FormAddScreenState();
}

class _FormAddScreenState extends State<FormAddScreen> {
  bool _isLoading = false;
  ApiService _apiService = ApiService();
  bool _isFieldNameValid;
  bool _isFieldTypeValid;
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerType = TextEditingController();

  @override
  void initState() {
    if (widget.fds != null) {
      _isFieldNameValid = true;
      _controllerName.text = widget.fds.nama;
      _isFieldTypeValid = true;
      _controllerType.text = widget.fds.type;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          widget.fds == null ? "FORM ADD" : "CHANGE DATA",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _buildTextFieldName(),
                _buildTextFieldType(),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: RaisedButton(
                    child: Text(
                      widget.fds == null
                          ? "SUBMIT".toUpperCase()
                          : "UPDATE".toUpperCase(),
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    onPressed: () {
                      if (_isFieldNameValid == null ||
                          _isFieldTypeValid == null ||
                          !_isFieldNameValid ||
                          !_isFieldTypeValid) {
                        _scaffoldState.currentState.showSnackBar(
                          SnackBar(
                            content: Text("Please fill all field"),
                          ),
                        );
                        return;
                      }
                      setState(() => _isLoading = true);
                      String name = _controllerName.text.toString();
                      String type = _controllerType.text.toString();
                      Food fd = Food(nama: name, type: type);
                      if (widget.fds == null) {
                        _apiService.createFood(fd).then((isSuccess) {
                          setState(() => _isLoading = false);
                          if (isSuccess) {
                            Navigator.pop(_scaffoldState.currentState.context);
                          } else {
                            _scaffoldState.currentState.showSnackBar(SnackBar(
                              content: Text("Submit data failed"),
                            ));
                          }
                        });
                        print(fd);
                      } else {
                        fd.id = widget.fds.id;
                        _apiService.updateFood(fd).then((isSuccess) {
                          setState(() => _isLoading = false);
                          if (isSuccess) {
                            Navigator.pop(_scaffoldState.currentState.context);
                          } else {
                            _scaffoldState.currentState.showSnackBar(SnackBar(
                              content: Text("Update data failed"),
                            ));
                          }
                        });
                      }
                    },
                    color: Colors.orange[600],
                  ),
                )
              ],
            ),
          ),
          _isLoading
              ? Stack(
                  children: <Widget>[
                    Opacity(
                      opacity: 0.3,
                      child: ModalBarrier(
                        dismissible: false,
                        color: Colors.grey,
                      ),
                    ),
                    Center(
                      child: CircularProgressIndicator(),
                    ),
                  ],
                )
              : Container(),
        ],
      ),
    );
  }

  Widget _buildTextFieldName() {
    return TextField(
      controller: _controllerName,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "NAME",
        errorText: _isFieldNameValid == null || _isFieldNameValid
            ? null
            : "Name is required",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldNameValid) {
          setState(() => _isFieldNameValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldType() {
    return TextField(
      controller: _controllerType,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "TYPE",
        errorText: _isFieldTypeValid == null || _isFieldTypeValid
            ? null
            : "Type is required",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldTypeValid) {
          setState(() => _isFieldTypeValid = isFieldValid);
        }
      },
    );
  }
}
