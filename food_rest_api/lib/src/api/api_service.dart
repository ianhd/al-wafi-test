import 'package:food_rest_api/src/model/food.dart';
import 'package:http/http.dart' show Client;

class ApiService {
  final String baseUrl = "http://10.0.2.2:8000/api";
  Client client = Client();

  Future<List<Food>> getFood() async {
    final response = await client.get("$baseUrl/food");
    if (response.statusCode == 200) {
      return foodFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<bool> createFood(Food data) async {
    final response = await client.post(
      "$baseUrl/food",
      headers: {"content-type": "application/json"},
      body: foodToJson(data),
    );
    print(response.body);
    if (response.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateFood(Food data) async {
    final response = await client.put(
      "$baseUrl/food/${data.id}",
      headers: {"content-type": "application/json"},
      body: foodToJson(data),
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> deleteFood(int id) async {
    final response = await client.delete(
      "$baseUrl/food/$id",
      headers: {"content-type": "application/json"},
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
